SECRET CHAMBER AT MOUNT RUSHMORE
This sounds like something out of a plot of a hollywood movie, but the chamber really exists. 
It can be found behind the Abraham Lincoln and was designed  to store important documents. 
This document contains a list of translation letters. 
some letters may have morethan one translation. some may have no translation. 
If they have same length, they process letter by letter. 
If words have different length translation not possible. 
The possible translations of letters and a list of pairs of original and decipherched word. 
Mainly our task is to verify whether the words in each pair match. 
INPUT:The first line of input contains two integers m and n,  where  m is the number of translations of letters and n is the number of word pairs. Each of the next m lines contains two distinct space separated letters a and b,  indicating that the letter a can be translated to the b. each ordered pairs of letters (a, b) appears at most once. following this are n times each containing a word pair to check. 
OUTPUT:For each pair of words, display yes if the two words match, and it display no if two words doesn't match. 

TEAM 89:

Sravya-20wh1a1264         
Deekshitha-20wh1a0242     
Keerthi-20wh1a0439         
Deepika-20wh1a0585         
Nishkala-20wh1a12B8        
Shilpa-21wh5a0518         

